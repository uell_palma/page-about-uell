import angular from 'angular'
import '@uirouter/angularjs'
import '../scss/style.scss'

const app = angular.module('app', ['ui.router'])

app.controller('appController',  ['$scope', '$http', function ($scope, $http) {
  $scope.title = 'About UELL'

  $scope.info = {
    title: 'Esse sou eu, e essa página é sobre minha carreira profissional.',
    desc: 'Olá, eu me chamo Uellingotn Palma tenho 24 anos e sou formado em Publicidade, mas nunca exerci a profissão, pois exerço atualmente outra ocupação que me surpreende e me motiva a ser cada vez melhor no que faço, sou desenvolvedor web desde os 16 anos, desde então procuro sempre evoluir e aprender coisas novas.'
  }

  $scope.contacts = [
    {
      title: 'Email',
      tag: 'at',
      value: 'mailto:uell.palma@gmail.com'
    },
    {
      title: 'LinkedIn',
      tag: 'linkedin',
      value: 'www.linkedin.com/in/uell-palma'
    },
    {
      title: 'Behance',
      tag: 'behance',
      value: 'https://www.behance.net/uell_palma'
    },
    {
      title: 'Telefone',
      tag: 'mobile',
      value: 'tel:+5571993966166'
    },
  ]

  $scope.skills = [
    {
      title: 'Front End Developer (PLENO)',
      desc: 'Javascript, CSS, HTML (e derivados)',
    },
    {
      title: 'Back End Developer (PLENO)',
      desc: 'PHP (Laravel e Codeigniter), MySql',
    },
    {
      title: 'Mobile (PLENO)',
      desc: 'React Native',
    },
    {
      title: 'Designer de Interface, UI',
      desc: 'Figma, Adobe XD',
    },
    {
      title: 'Git',
      desc: '',
    },
    {
      title: 'Pacote Adobe',
      desc: 'Photoshop, Adobe XD',
    },
    {
      title: 'Metodologias Ageis',
      desc: 'Scrum',
    },
  ]

  $scope.series = [
    {
      title: 'Breaking Bad',
      cover: 'https://m.media-amazon.com/images/M/MV5BMjhiMzgxZTctNDc1Ni00OTIxLTlhMTYtZTA3ZWFkODRkNmE2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY268_CR5,0,182,268_AL_.jpg'
    },
    {
      title: 'Vikings',
      cover: 'https://m.media-amazon.com/images/M/MV5BNDYyNzk1NzYwOF5BMl5BanBnXkFtZTgwMTQ0Nzc4MzI@._V1_UY268_CR8,0,182,268_AL_.jpg'
    },
    {
      title: 'Westworld',
      cover: 'https://m.media-amazon.com/images/M/MV5BNThjM2Y3MDUtYTIyNC00ZDliLWJlMmItNWY1N2E5NjhmMGM4XkEyXkFqcGdeQXVyNjU2ODM5MjU@._V1_UX182_CR0,0,182,268_AL_.jpg'
    },
    {
      title: 'Silicon Valley',
      cover: 'https://m.media-amazon.com/images/M/MV5BMTAxNTEyODE5MTNeQTJeQWpwZ15BbWU4MDE3MjM3ODQz._V1_UX182_CR0,0,182,268_AL_.jpg'
    },
    {
      title: 'Billions',
      cover: 'https://m.media-amazon.com/images/M/MV5BMTU1NjM5NjEzMF5BMl5BanBnXkFtZTgwMjE3NzU0NzM@._V1_UX182_CR0,0,182,268_AL_.jpg'
    },
    {
      title: 'The Walking Dead',
      cover: 'https://m.media-amazon.com/images/M/MV5BNDM2OTA1NzIzMV5BMl5BanBnXkFtZTgwOTkxMTk5NjM@._V1_UX182_CR0,0,182,268_AL_.jpg'
    },
    {
      title: 'Stanger Things',
      cover: 'https://m.media-amazon.com/images/M/MV5BMTgwNDAxNTQ4Ml5BMl5BanBnXkFtZTgwOTAyMjQ2NzM@._V1_UY268_CR3,0,182,268_AL_.jpg'
    },
    {
      title: 'Dark',
      cover: 'https://m.media-amazon.com/images/M/MV5BMTUzNjQ2MTY5NV5BMl5BanBnXkFtZTgwOTAzNTQxNDM@._V1_UX182_CR0,0,182,268_AL_.jpg'
    },
    {
      title: 'House of Cards',
      cover: 'https://m.media-amazon.com/images/M/MV5BODM1MDU2NjY5NF5BMl5BanBnXkFtZTgwMDkxNTcwNjM@._V1_UX182_CR0,0,182,268_AL_.jpg'
    },
    {
      title: 'Atlanta',
      cover: 'https://m.media-amazon.com/images/M/MV5BNzk5NDE5NDY1Ml5BMl5BanBnXkFtZTgwNTk0MTc3NDM@._V1_UX182_CR0,0,182,268_AL_.jpg'
    }
  ]

  $scope.games = [
    {
      title: 'Fifa',
      cover: 'https://static-cdn.jtvnw.net/ttv-boxart/FIFA%2019.jpg'
    },
    {
      title: 'Apex Lengeds',
      cover: 'https://upload.wikimedia.org/wikipedia/pt/a/ad/Apex_legends_capa.jpg'
    },
    {
      title: 'Batlefield',
      cover: 'https://upload.wikimedia.org/wikipedia/pt/c/c9/Battlefield_1_capa.jpg'
    },
    {
      title: 'Forza',
      cover: 'https://store-images.s-microsoft.com/image/apps.20615.14094456225993959.2d017079-463a-4bd6-ac7a-2fb4f65673e9.0faeefd3-4ad9-4634-98df-75b9aeb92d48'
    },
    {
      title: 'Tintfall',
      cover: 'https://images-eds-ssl.xboxlive.com/image?url=8Oaj9Ryq1G1_p3lLnXlsaZgGzAie6Mnu24_PawYuDYIoH77pJ.X5Z.MqQPibUVTcxnI11TtsqwQ5cHnbzLWojXw3fhQkTs2B4atbEqmYCp_YkqPF6B366O3cA__ZXvFQpSzgWEGwe1Ch7agWk89X0NWQOQR08pNU2q_cZnbiz0hYWHbsTip9_RI0bhqsyIcIkJYrSxONAOIMpBTpevHLbaizD1yhMfjXQasEbn9.xdA-&w=200&h=300&format=jpg'
    }
  ]

  $scope.books = [
    {
      title: 'Sapiens',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/41uzfhT987L._SX299_BO1,204,203,200_.jpg'
    },
    {
      title: 'Homo Deus',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/51puLkDrLlL._SX351_BO1,204,203,200_.jpg'
    },
    {
      title: 'Scrum',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/514ZCPRQ-bL._SX337_BO1,204,203,200_.jpg'
    },
    {
      title: 'De Zero a Um',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/419MiydHgiL._SX346_BO1,204,203,200_.jpg'
    },
    {
      title: 'A Startup Enxuta',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/51FqHGBqOcL._SX330_BO1,204,203,200_.jpg'
    },
    {
      title: 'O Poder do Hábito',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/51VFNJGBvqL._SX346_BO1,204,203,200_.jpg'
    },
    {
      title: 'As Armas da Persuasão',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/51ZeaW0F%2BML._SX344_BO1,204,203,200_.jpg'
    },
    {
      title: 'Criatividade S.A.',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/41wUZd5xJxL._SX346_BO1,204,203,200_.jpg'
    },
    {
      title: 'A Caulda Longa',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/41mET8GeUIL._SX343_BO1,204,203,200_.jpg'
    },
    {
      title: 'Free',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/51kW0UJxdAL._SX346_BO1,204,203,200_.jpg'
    },
    {
      title: 'A Estratégia do Oceano Azul',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/51F78fQN7VL._SX347_BO1,204,203,200_.jpg'
    },
    {
      title: 'Nada Easy',
      cover: 'https://images-na.ssl-images-amazon.com/images/I/51aElurSf-L.jpg'
    }
  ]

  $scope.projects = [
    {
      title: 'Singelo',
      tag: 'Programação e Layout',
      desc: 'Aplicativo que conecta profissionais de serviço a clientes. Desenvolvimento de API com Laravel PHP, e aplicativos desenvolvidos com React Native.',
      href: 'https://singelo.com.br',
    },
    {
      title: 'Diário Paralelo',
      tag: 'Programação e Layout',
      desc: 'Site de notícias regional. Desenvolvido em Wordpress.',
      href: 'http://www.diarioparalelo.com.br',
    },
    {
      title: 'Faculdade Facemp',
      tag: 'Programação',
      desc: 'Site Institucional da Faculdade FACEMP',
      href: 'https://facemp.edu.br',
    },
    {
      title: 'Bahia Transfer',
      tag: 'Programação',
      desc: 'Site Institucional da Faculdade FACEMP',
      href: 'https://br.bahiatransfer.com.br/',
    },
    {
      title: 'Bli',
      tag: 'Front-End',
      desc: 'Site de imoveis para aluguel e venda, desenvolvido com Wordpress',
      href: 'https://www.bli.com.br/',
    },
    {
      title: 'Ventiladores e Cadeiras',
      tag: 'Programação',
      desc: 'Site Institucional da Faculdade FACEMP',
      href: 'https://www.ventiladoresecadeiras.com.br/',
    },
    {
      title: 'Mural de Aventuras',
      tag: 'Programação e Layout',
      desc: 'Sistema web voltado para grupo de ciclistas aventureiros. Desenvolvido em Codeigniter PHP',
      href: 'http://muraldeaventuras.com.br/',
    }
  ]

  $scope.career = [
    {
      year: '2018 - 2019',
      title: 'Singelo.com.br',
      tag: 'Programador e Fundador',
      desc: 'Durante 2018, decedir empreender novamente, lancei um MVP de um serviço de marketplace que conecta profissionais de serviços, validei a proposta de valor e decidi aprender React Native para construir aplicativos mobile, com mais recursos.',
    },
     {
      year: '2018',
      title: 'Freelancer',
      tag: 'Programador',
      desc: 'Com o crescimento do meu network, surgem novos trabalhos em outras agências digitais, com isso, abro mão dos estágios e passo realizar trabalhos como freelancer.',
    },
    {
      year: '2017',
      title: 'Abrasivo Digital',
      tag: 'Desenvolvedor Pleno',
      desc: 'Passo a trabalhar em dois estágio, sendo HomeOffice, o que otimizou meu tempo o que fez com que sobrasse tempo para ambos.',
    },
    {
      year: '2016',
      title: 'Loup Br',
      tag: 'Desenvolvedor Pleno',
      desc: 'Volto a ser estagiário em uma agência digital, dessa vez para desenvolver sistemas web e sites com wordpress.',
    },
    {
      year: '2015',
      title: 'Toca Música',
      tag: 'Empreender',
      desc: 'No final de 2015, deixo o estágio para empreender, crio um sistema de streaming e download de músicas para bandas independentes da Bahia, algo parecido com o Spotify.',
    },
    {
      year: '2015',
      title: 'Abrasivo Digital',
      tag: 'Estagiário - Web Design',
      desc: 'Inicio meu primeiro estágio, mas não como publicitário e sim como desenvolvedor web, mais precisamente programador Wordpress.',
    },
    {
      year: '2014',
      title: 'Início de Faculdade',
      tag: 'Publicidade e Propaganda',
      desc: 'Ingresso na faculdade Estácio para cursar Publicidade e Propaganda, motivado pelo desejo de criação e desenho.',
    },
    {
      year: '2012',
      title: 'Diário Paralelo',
      tag: 'Programador e Co-Fundador',
      desc: 'Ainda quando morava no interior, juntamente com o meu tio, ajudei a fundar e desenvolver um do melhores sites de notícias da região, anteriomente chamado de Gandu Notícias, mudamos o nome para ter uma abrangência e atingir um público maior',
    }
  ]

  // get list following artists spotify
  let token = 'BQAhBPewqothzTGHXbvDj3Mmy6NoxFn_1QJXui6Eb1Xt6enMp0A0ZYM_qHYH5e-tFztAfFWja9dRJebFB6RGumlAIUsTOpItYYVs3afZ_QX_mzDnn7DdG5dE2w1pNGNQfWMYK3MnkNrr8d9a3otBoMQ'
  var req = {
    method: 'GET',
    url: 'https://api.spotify.com/v1/me/following',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    params: { 
      type: 'artist',
      after: '0I2XqVXqHScXjHhk6AYYRe',
      limit: 10
    }
  }
  $http(req).
    then((resp) => {
      $scope.artists = resp.data.artists.items
      console.log($scope.artists)
    }, err => {
      console.log(err)
    })
}])

app.config(function($stateProvider, $urlRouterProvider) {
  var aboutState = {
    name: 'about',
    url: '',
    template: require('../pages/about/about.html')
  }

  var projectsState = {
    name: 'projects',
    url: '/projects',
    template: require('../pages/projects/projects.html')
  }

  var careerState = {
    name: 'career',
    url: '/career',
    template: require('../pages/career/career.html')
  }

  var hobbiesState = {
    name: 'hobbies',
    url: '/hobbies',
    template: require('../pages/hobbies/hobbies.html')
  }

  $stateProvider.state(aboutState);
  $stateProvider.state(projectsState);
  $stateProvider.state(careerState);
  $stateProvider.state(hobbiesState);
});