# Page About Uell

This is a web page about my professional trajectory, for educational purposes.

* AngularJs.
* Webpack
* ES6, support with babel.
* SASS.

### Quick start

```bash
# clone our repo
$ git clone https://uell_palma@bitbucket.org/uell_palma/page-about-uell.git

# change directory to app
$ cd page-about-uell

# install the dependencies with npm
$ npm install

# start the server
$ npm dev

# for build
$ npm build
```

go to [http://localhost:8080](http://localhost:8080) in your browser.

# License

[MIT](/LICENSE)