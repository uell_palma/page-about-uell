const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const devMode = process.env.NODE_ENV !== 'production'

module.exports = {
	mode: 'development',
	entry: './src/js/index.js',
	output: {
		filename: 'main.js',
		path: path.resolve(__dirname, 'dist/js')
	},
	devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    hot: true,
    open: true,
    port: 8080
  },
  module: {
    rules: [
	    {
	      test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'development',
            },
          },
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
		  },
		  {
			  test: /\.(html)$/,
			  use: {
			    loader: 'html-loader',
			    options: {
			      attrs: [':data-src']
			    }
			  }
			},
			{
	      test: /\.m?js$/,
	      exclude: /(node_modules)/,
	      use: {
	        loader: 'babel-loader',
	        options: {
	          // presets: ["@babel/preset-env"]
	          presets: [path.resolve(__dirname, 'node_modules/@babel/preset-env')]
	        }
	      }
	    }
	  ]
  },
  plugins: [
  	new MiniCssExtractPlugin({
      filename: '../css/[name].css',
    })
  ],
  optimization: {
    minimizer: [
    	new UglifyJsPlugin({
    		test: /\.js(\?.*)?$/i,
    	})
    ],
  },
}